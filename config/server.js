module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 80),
  url: env('APP_URL', 'https://cms-strapi-v1.herokuapp.com/'),
  app: {
    keys: env.array('APP_KEYS'),
  },
});

